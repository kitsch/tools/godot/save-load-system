class_name SlotSaveSystem
extends Object
# A save/load system with a limited number of slots.


const SavingInterface = preload("./classes/saving_interface.gd")

var filename = "slot_{index}"
var interface = SavingInterface.new()
var slot_count := 3


# Builds the path to a save slot.
func build_path(slot: int):
	assert(slot >= 0 or slot < slot_count, "Slot out of range.")
	return filename.format({ "index": slot })


# Clears an existing slot.
func clear_slot(slot: int) -> bool:
	return interface.delete_file(build_path(slot))


# Gets the fill status of all slots.
func get_all_slots() -> Array[bool]:
	var arr: Array[bool] = []
	for slot in slot_count:
		arr.append(slot_filled(slot))
	return arr


# Loads every slot.
func load_all_slots() -> Array[Object]:
	var arr: Array[Object] = []
	for slot in slot_count:
		arr.append(load_slot(slot) if slot_filled(slot) else null)
	return arr


# Loads the slot.
func load_slot(slot: int) -> Object:
	return interface.load_file(build_path(slot))


# Saves to the slot.
func save_slot(slot: int, data: Dictionary) -> Object:
	return interface.save_file(build_path(slot), data)


# Checks to see if the slot is filled.
func slot_filled(slot: int) -> bool:
	return interface.file_exists(build_path(slot))
