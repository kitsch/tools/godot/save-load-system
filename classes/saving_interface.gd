extends Object
# Facilitates saving and loading a save file.


var dir := "user://saves"
var ext := "save"
var template := "{dir}/{filename}.{ext}"


func _init(overrides := {}):
	# Use overrides if they exist.
	if overrides.has("dir"):
		dir = overrides.dir
	if overrides.has("ext"):
		ext = overrides.ext
	if overrides.has("template"):
		template = overrides.template

	# Create saves directory if doesn't exist.
	if not DirAccess.dir_exists_absolute(dir):
		DirAccess.make_dir_absolute(dir)


# Builds a save file path.
func build_path(filename: String) -> String:
	return template.format({
		"dir": dir,
		"ext": ext,
		"filename": filename,
	})


# Deletes an existing save file.
func delete_file(filename: String) -> bool:
	var path = build_path(filename)

	if FileAccess.file_exists(path):
		DirAccess.remove_absolute(path)
		return true

	return false


# Returns whether a save file exists.
func file_exists(filename: String) -> bool:
	return FileAccess.file_exists(build_path(filename))


# Returns all filenames which exist in the save directory.
func get_all_files() -> Array:
	var files = Array(DirAccess.get_files_at(dir))
	return files.filter(func (file: String):
		return file.ends_with(".%s" % ext)
	).map(func (file: String):
		return file.substr(0, file.length() - ext.length() - 1)
	)


# Loads a save, returning an error if it fails.
func load_file(filename: String) -> Object:
	var path = build_path(filename)

	if not FileAccess.file_exists(path):
		return SaveLoadSystemError.new("Save file not found.")

	var data = FileAccess.get_file_as_string(path)
	return SaveLoadSystemSuccess.new(JSON.parse_string(data))


# Saves to a file.
func save_file(filename: String, data: Dictionary) -> Object:
	var path = build_path(filename)
	var output = JSON.stringify(data)

	# Ensure file does NOT exist.
	if FileAccess.file_exists(path):
		return SaveLoadSystemError.new("Save file already exists.")

	var file = FileAccess.open(path, FileAccess.WRITE)

	if not file:
		return SaveLoadSystemError.new("Failed to save file.")

	file.store_string(output)

	return SaveLoadSystemSuccess.new("Success.")
