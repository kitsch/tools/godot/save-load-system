class_name SaveLoadSystemError
extends Object
# Contains all the information for an error in the SaveLoadSystem.


var message: String


func _init(given_message: String):
	message = given_message
