class_name SingularSaveSystem
extends Object
# A save/load system with only one save file.


const SavingInterface = preload("./classes/saving_interface.gd")

var filename = "data"
var interface = SavingInterface.new()


# Deletes the existing save file.
func delete_file() -> bool:
	return interface.delete_file(filename)


# Checks to see if the save file exists.
func file_exists() -> bool:
	return interface.file_exists(filename)


# Loads the save file.
func load_file() -> Object:
	return interface.load_file(filename)


# Saves to the file.
func save_file(data: Dictionary, overwrite = false) -> Object:
	if overwrite and file_exists():
		if not delete_file():
			return SaveLoadSystemError.new("Couldn't delete a non-existent save file.")

	return interface.save_file(filename, data)
