class_name OrderedSaveSystem
extends Object
# A save/load system with many save files in save order (most recent first).


const SavingInterface = preload("./classes/saving_interface.gd")

var interface = SavingInterface.new()
var maximum_saves: int = 1000
var template: String = "{datetime} {title}"


# Deletes an existing save file.
func delete_file(filename: String) -> bool:
	return interface.delete_file(filename)


# Checks to see if a given save file exists.
func file_exists(filename: String) -> bool:
	return interface.file_exists(filename)


# Returns all save files in proper order.
func get_all_files() -> Array:
	var files = interface.get_all_files()
	files.reverse()
	return files


# Loads a file with a given filename.
func load_file(filename: String) -> Object:
	return interface.load_file(filename)


# Parses a given filename to datetime and title.
func parse_filename(filename: String) -> Dictionary:
	var split := Array(filename.split(" "))
	return {
		"datetime": _parse_datetime(split.pop_front()),
		"title": " ".join(split),
	}


# Saves a new file.
func save_file(title: String, data: Dictionary) -> Object:
	if interface.get_all_files().size() >= maximum_saves:
		return SaveLoadSystemError.new("Too many save files - Maximum of %s" % maximum_saves)

	var filename = template.format({
		"datetime": Time.get_datetime_string_from_system().replace(":", "-"),
		"title": title,
	})
	return interface.save_file(filename, data)


# Parses the datetime from saveable format to Dictionary.
func _parse_datetime(input: String) -> Dictionary:
	var date_and_time = Array(input.split("T"))
	date_and_time[1] = date_and_time[1].replace("-", ":")
	var datetime = String("T").join(date_and_time)
	return Time.get_datetime_dict_from_datetime_string(datetime, false)
